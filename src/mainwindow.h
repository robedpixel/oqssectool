#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "kyberkemencoder.h"
#include "ntrukemencoder.h"
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMainWindow>
#include <QMessageBox>
#include <QString>
#include <immintrin.h>
#include <intrin.h>
#include <openssl/evp.h>
#include <oqs/oqs.h>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  enum ENCTYPE { NTRU, KYBER };
  enum KEYTYPE { PUBLICKEY, PRIVATEKEY };
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  QByteArray public_key;
  QByteArray secret_key;
  QByteArray shared_secret;
  QByteArray plaintext;
  QByteArray ciphertext;
  QByteArray aad_IV_temp;
private slots:
  void on_GenerateKeyButton_clicked();

  void on_encodeButton_clicked();

  void on_decodeButton_clicked();

  void on_AESencodeButton_clicked();

  void on_AESdecodeButton_clicked();

  void on_savepubkeyButton_clicked();

  void on_readpubkeyButton_clicked();

  void on_saveprikeyButton_clicked();

  void on_readprikeyButton_clicked();

  void on_encryptandsaveButton_clicked();

  void on_readanddecryptButton_clicked();

  void on_publickeyTextEdit_textChanged();

  void on_privatekeyTextEdit_textChanged();

  void on_algoComboBox_currentIndexChanged(int index);

private:
  int gcm_encrypt(unsigned char *plaintext, int plaintext_len,
                  unsigned char *aad, int aad_len, unsigned char *key,
                  unsigned char *iv, int iv_len, unsigned char *ciphertext,
                  unsigned char *tag, bool *err);
  int gcm_decrypt(unsigned char *ciphertext, int ciphertext_len,
                  unsigned char *aad, int aad_len, unsigned char *tag,
                  unsigned char *key, unsigned char *iv, int iv_len,
                  unsigned char *plaintext, bool *err);
  int chacha20_poly1305_encrypt(unsigned char *plaintext, int plaintext_len,
                  unsigned char *aad, int aad_len, unsigned char *key,
                  unsigned char *iv, int iv_len, unsigned char *ciphertext,
                  unsigned char *tag, bool *err);
  int chacha20_poly1305_decrypt(unsigned char *ciphertext, int ciphertext_len,
                  unsigned char *aad, int aad_len, unsigned char *tag,
                  unsigned char *key, unsigned char *iv, int iv_len,
                  unsigned char *plaintext, bool *err);
  std::vector<AsyncKEMEncoder *> algoarray;
  bool p_err;
  void displayError(QString errormessage);
  Ui::MainWindow *ui;
  AsyncKEMEncoder *asyncencoder;
  OQS_STATUS rc;
  BIO *bio;
};
#endif // MAINWINDOW_H
