#include "asynckemencoder.h"
AsyncKEMEncoder::~AsyncKEMEncoder() { return; }
void AsyncKEMEncoder::GenerateKeypair(QByteArray *publickey,
                                      QByteArray *privatekey) {
  return;
}
void AsyncKEMEncoder::GenerateKeypair_Secure(QByteArray *publickey,
                                             QByteArray *privatekey) {
  return;
}
void AsyncKEMEncoder::GenerateandEncodeSymKey(const QByteArray &publickey,
                                              const QByteArray &ciphertext,
                                              QByteArray *shared_secret) {
  return;
}
void AsyncKEMEncoder::GenerateandEncodeSymKey_Secure(
    const QByteArray &publickey, QByteArray *ciphertext,
    QByteArray *shared_secret) {
  return;
}
void AsyncKEMEncoder::DecodeSymKey(const QByteArray &privatekey,
                                   const QByteArray &ciphertext,
                                   QByteArray *shared_secret) {
  return;
}
void AsyncKEMEncoder::DecodeSymKey_Secure(const QByteArray &privatekey,
                                          QByteArray *ciphertext,
                                          QByteArray *shared_secret) {
  return;
}
unsigned int AsyncKEMEncoder::GetPublicKeyLength() { return 0; }
unsigned int AsyncKEMEncoder::GetPrivateKeyLength() { return 0; }
unsigned int AsyncKEMEncoder::GetSymKeyLength() { return 0; }
unsigned int AsyncKEMEncoder::GetSymCipherTextLength() { return 0; }
QString AsyncKEMEncoder::GetFileExtension() { return 0; }
