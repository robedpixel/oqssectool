#ifndef ASYNCKEMENCODER_H
#define ASYNCKEMENCODER_H
#include <QByteArray>
#include <QDebug>
#include <QString>
#include <oqs/oqs.h>

class AsyncKEMEncoder {
public:
  OQS_STATUS rc;
  virtual ~AsyncKEMEncoder();
  virtual void GenerateKeypair(QByteArray *publickey, QByteArray *privatekey);
  virtual void GenerateKeypair_Secure(QByteArray *publickey,
                                      QByteArray *privatekey);
  virtual void GenerateandEncodeSymKey(const QByteArray &publickey,
                                       const QByteArray &ciphertext,
                                       QByteArray *shared_secret);
  virtual void GenerateandEncodeSymKey_Secure(const QByteArray &publickey,
                                              QByteArray *ciphertext,
                                              QByteArray *shared_secret);
  virtual void DecodeSymKey(const QByteArray &privatekey,
                            const QByteArray &ciphertext,
                            QByteArray *shared_secret);
  virtual void DecodeSymKey_Secure(const QByteArray &privatekey,
                                   QByteArray *ciphertext,
                                   QByteArray *shared_secret);
  virtual unsigned int GetPublicKeyLength();
  virtual unsigned int GetPrivateKeyLength();
  virtual unsigned int GetSymKeyLength();
  virtual unsigned int GetSymCipherTextLength();
  virtual QString GetFileExtension();
};

#endif // ASYNCKEMENCODER_H
