#include "ntrukemencoder.h"

NtruKEMEncoder::NtruKEMEncoder() {}
NtruKEMEncoder::~NtruKEMEncoder() {}
void NtruKEMEncoder::GenerateKeypair(QByteArray *publickey,
                                     QByteArray *privatekey) {
  rc = OQS_KEM_ntruprime_sntrup761_keypair(
      reinterpret_cast<uint8_t *>(publickey->data()),
      reinterpret_cast<uint8_t *>(privatekey->data()));
  if (rc != OQS_SUCCESS) {
    qDebug() << "OQS ERROR!";
  }
}
void NtruKEMEncoder::GenerateKeypair_Secure(QByteArray *publickey,
                                            QByteArray *privatekey) {
  publickey->resize(GetPublicKeyLength());
  privatekey->resize(GetPrivateKeyLength());
  GenerateKeypair(publickey, privatekey);
}
void NtruKEMEncoder::GenerateandEncodeSymKey(const QByteArray &publickey,
                                             const QByteArray &ciphertext,
                                             QByteArray *shared_secret) {
  rc = OQS_KEM_ntruprime_sntrup761_encaps(
      const_cast<uint8_t *>(
          reinterpret_cast<const uint8_t *>(ciphertext.data())),
      reinterpret_cast<uint8_t *>(shared_secret->data()),
      const_cast<uint8_t *>(
          reinterpret_cast<const uint8_t *>(publickey.data())));
  if (rc != OQS_SUCCESS) {
    qDebug() << "OQS ERROR!";
  }
}
void NtruKEMEncoder::GenerateandEncodeSymKey_Secure(const QByteArray &publickey,
                                                    QByteArray *ciphertext,
                                                    QByteArray *shared_secret) {
  if (publickey.length() != GetPublicKeyLength()) {
    qDebug() << "public key wrong length!";
    return;
  }
  ciphertext->resize(GetSymCipherTextLength());
  shared_secret->resize(GetSymKeyLength());
  GenerateandEncodeSymKey(publickey, *ciphertext, shared_secret);
}
void NtruKEMEncoder::DecodeSymKey(const QByteArray &privatekey,
                                  const QByteArray &ciphertext,
                                  QByteArray *shared_secret) {
  rc = OQS_KEM_ntruprime_sntrup761_decaps(
      reinterpret_cast<uint8_t *>(shared_secret->data()),
      const_cast<uint8_t *>(
          reinterpret_cast<const uint8_t *>(ciphertext.data())),
      const_cast<uint8_t *>(
          reinterpret_cast<const uint8_t *>(privatekey.data())));
  if (rc != OQS_SUCCESS) {
    qDebug() << "OQS ERROR!";
  }
}
void NtruKEMEncoder::DecodeSymKey_Secure(const QByteArray &privatekey,
                                         QByteArray *ciphertext,
                                         QByteArray *shared_secret) {
  if (privatekey.length() != GetPrivateKeyLength()) {
    qDebug() << "private key wrong length!";
    return;
  }
  ciphertext->resize(GetSymCipherTextLength());
  shared_secret->resize(GetSymKeyLength());
  DecodeSymKey(privatekey, *ciphertext, shared_secret);
}
unsigned int NtruKEMEncoder::GetPublicKeyLength() {
  return OQS_KEM_ntruprime_sntrup761_length_public_key;
}
unsigned int NtruKEMEncoder::GetPrivateKeyLength() {
  return OQS_KEM_ntruprime_sntrup761_length_secret_key;
}
unsigned int NtruKEMEncoder::GetSymKeyLength() {
  return OQS_KEM_ntruprime_sntrup761_length_shared_secret;
}
unsigned int NtruKEMEncoder::GetSymCipherTextLength() {
  return OQS_KEM_ntruprime_sntrup761_length_ciphertext;
}
QString NtruKEMEncoder::GetFileExtension() { return QString("ntru"); }
