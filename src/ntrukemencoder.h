#ifndef NTRUKEMENCODER_H
#define NTRUKEMENCODER_H

#include "asynckemencoder.h"

class NtruKEMEncoder : public AsyncKEMEncoder {
public:
  NtruKEMEncoder();
  ~NtruKEMEncoder();
  void GenerateKeypair(QByteArray *publickey, QByteArray *privatekey);
  void GenerateKeypair_Secure(QByteArray *publickey, QByteArray *privatekey);
  void GenerateandEncodeSymKey(const QByteArray &publickey,
                               const QByteArray &ciphertext,
                               QByteArray *shared_secret);
  void GenerateandEncodeSymKey_Secure(const QByteArray &publickey,
                                      QByteArray *ciphertext,
                                      QByteArray *shared_secret);
  void DecodeSymKey(const QByteArray &privatekey, const QByteArray &ciphertext,
                    QByteArray *shared_secret);
  void DecodeSymKey_Secure(const QByteArray &privatekey, QByteArray *ciphertext,
                           QByteArray *shared_secret);
  unsigned int GetPublicKeyLength();
  unsigned int GetPrivateKeyLength();
  unsigned int GetSymKeyLength();
  unsigned int GetSymCipherTextLength();
  QString GetFileExtension();

private:
  using AsyncKEMEncoder::DecodeSymKey;
  using AsyncKEMEncoder::DecodeSymKey_Secure;
  using AsyncKEMEncoder::GenerateandEncodeSymKey;
  using AsyncKEMEncoder::GenerateandEncodeSymKey_Secure;
  using AsyncKEMEncoder::GenerateKeypair;
  using AsyncKEMEncoder::GenerateKeypair_Secure;
  using AsyncKEMEncoder::GetFileExtension;
  using AsyncKEMEncoder::GetPrivateKeyLength;
  using AsyncKEMEncoder::GetPublicKeyLength;
  using AsyncKEMEncoder::GetSymCipherTextLength;
  using AsyncKEMEncoder::GetSymKeyLength;
};

#endif // NTRUKEMENCODER_H
