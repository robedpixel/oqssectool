#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  aad_IV_temp.resize(12);
  algoarray = {new NtruKEMEncoder(), new KyberKEMEncoder()};
  for (std::vector<AsyncKEMEncoder *>::size_type i = 0; i < algoarray.size();
       i++) {
    ui->algoComboBox->addItem(algoarray[i]->GetFileExtension());
  }
  ui->algoComboBox->setCurrentIndex(0);
}

MainWindow::~MainWindow() {
  ui->publickeyTextEdit->setPlainText("");
  ui->privatekeyTextEdit->setPlainText("");
  ui->AESplainTextEdit->setPlainText("");
  ui->tagTextEdit->setPlainText("");
  ui->AEScipherTextEdit->setPlainText("");
  ui->sharedTextEdit->setPlainText("");
  ui->ciphertextTextEdit->setPlainText("");
  public_key.clear();
  secret_key.clear();
  shared_secret.clear();
  plaintext.clear();
  ciphertext.clear();
  aad_IV_temp.clear();
  asyncencoder = nullptr;
  for (std::vector<AsyncKEMEncoder *>::size_type i = 0; i < algoarray.size();
       i++) {
    delete algoarray[i];
  }
  delete asyncencoder;
  delete ui;
}

void MainWindow::on_GenerateKeyButton_clicked() {
  asyncencoder->GenerateKeypair(&public_key, &secret_key);
  ui->publickeyTextEdit->setPlainText(public_key.toBase64());
  ui->privatekeyTextEdit->setPlainText(secret_key.toBase64());
}

void MainWindow::on_encodeButton_clicked() {
  if (ui->publickeyTextEdit->document()->isEmpty()) {
    displayError("Please generate a public key before encoding key ciphertext");
  } else {
    QByteArray temp =
        QByteArray::fromBase64(ui->publickeyTextEdit->toPlainText().toUtf8());
    asyncencoder->GenerateandEncodeSymKey(temp, ciphertext, &shared_secret);
    ui->ciphertextTextEdit->setPlainText(ciphertext.toBase64());
    ui->sharedTextEdit->setPlainText(shared_secret.toBase64());
    ui->AESplainTextEdit->setPlainText("");
    ui->tagTextEdit->setPlainText("");
    ui->AEScipherTextEdit->setPlainText("");
  }
}

void MainWindow::on_decodeButton_clicked() {
  QByteArray temp =
      QByteArray::fromBase64(ui->privatekeyTextEdit->toPlainText().toUtf8());
  plaintext =
      QByteArray::fromBase64(ui->ciphertextTextEdit->toPlainText().toUtf8());
  asyncencoder->DecodeSymKey(temp, plaintext, &shared_secret);
  ui->sharedTextEdit->setPlainText(shared_secret.toBase64());
}

void MainWindow::on_AESencodeButton_clicked() {
  QByteArray temp = ui->AESplainTextEdit->toPlainText().toUtf8();
  QByteArray shared_temp =
      QByteArray::fromBase64(ui->sharedTextEdit->toPlainText().toUtf8());
  QByteArray ciphertexttemp;
  int ciphertext_length;
  QByteArray tag_temp;
  tag_temp.resize(16);
  tag_temp.fill(0, 16);
  ciphertexttemp.resize(temp.length() * 5);
  for (int i = 0; i < 12; i++) {
    aad_IV_temp.data()[i] = shared_temp[31 - i];
  }
  ciphertext_length =
      gcm_encrypt(reinterpret_cast<unsigned char *>(temp.data()), temp.length(),
                  reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 0,
                  reinterpret_cast<unsigned char *>(shared_temp.data()),
                  reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 12,
                  reinterpret_cast<unsigned char *>(ciphertexttemp.data()),
                  reinterpret_cast<unsigned char *>(tag_temp.data()), &p_err);
  if (p_err == false) {
    ciphertexttemp.resize(ciphertext_length);
    ui->tagTextEdit->setPlainText(tag_temp.toBase64());
    ui->AEScipherTextEdit->setPlainText(ciphertexttemp.toBase64());
  } else {
    displayError("Error encrypting!");
  }
}

void MainWindow::on_AESdecodeButton_clicked() {
  QByteArray temp =
      QByteArray::fromBase64(ui->AEScipherTextEdit->toPlainText().toUtf8());
  QByteArray shared_temp =
      QByteArray::fromBase64(ui->sharedTextEdit->toPlainText().toUtf8());
  QByteArray tag_temp =
      QByteArray::fromBase64(ui->tagTextEdit->toPlainText().toUtf8());
  QByteArray plaintexttemp;
  int plaintext_length;
  plaintexttemp.resize(temp.length() * 5);
  plaintext_length = gcm_decrypt(
      reinterpret_cast<unsigned char *>(temp.data()), temp.length(),
      reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 0,
      reinterpret_cast<unsigned char *>(tag_temp.data()),
      reinterpret_cast<unsigned char *>(shared_temp.data()),
      reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 12,
      reinterpret_cast<unsigned char *>(plaintexttemp.data()), &p_err);
  if (p_err == false) {
    plaintexttemp.resize(plaintext_length);
    ui->AESplainTextEdit->setPlainText(QString::fromUtf8(plaintexttemp.data()));
  } else {
    displayError("Error decrypting!");
  }
}

void MainWindow::on_savepubkeyButton_clicked() {
  if (ui->publickeyTextEdit->document()->isEmpty() == false) {
    QByteArray pubkeytemp = ui->publickeyTextEdit->toPlainText().toUtf8();
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("Save Public Key"), "",
        QString("oqssec") + " file (*." + QString("oqssec") +
            ");;All Files (*)");
    if (!fileName.isEmpty()) {
      QFile file(fileName);
      if (file.open(QIODevice::WriteOnly)) {
        qDebug() << "saving";
        QJsonObject jsonObject;
        jsonObject.insert("enctype",
                          QString::number(ui->algoComboBox->currentIndex()));
        jsonObject.insert("keytype", QString::number(MainWindow::PUBLICKEY));
        jsonObject.insert("value", QString(pubkeytemp));
        QJsonDocument doc(jsonObject);
        QTextStream stream(&file);
        stream << doc.toJson();
        file.close();
      }
    }
  } else {
    displayError("No public key generated!");
  }
}

void MainWindow::on_readpubkeyButton_clicked() {
  QByteArray rawjs;
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Read Public Key"), "",
      QString("oqssec") + " file (*." + QString("oqssec") + ");;All Files (*)");
  if (!fileName.isEmpty()) {
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
      qDebug() << "reading";
      QTextStream stream(&file);
      rawjs = stream.readAll().toUtf8();
      file.close();
    }
    // read enctype and set index to it
    QJsonDocument json = QJsonDocument::fromJson(rawjs);
    QJsonObject jsonObject = json.object();
    unsigned int enctype = jsonObject["enctype"].toString().toUInt();
    if (enctype > 1) {
      displayError("Wrong encoder type!");
      return;
    }
    ui->algoComboBox->setCurrentIndex(enctype);
    QString keytype = jsonObject["keytype"].toString();
    if (keytype.toUInt() != PUBLICKEY) {
      displayError("key is not public key!");
      return;
    }
    QByteArray pubkeytemp = jsonObject["value"].toString().toUtf8();
    // read in pubkeytemp if keytype is valid
    if (QByteArray::fromBase64(pubkeytemp).length() ==
        asyncencoder->GetPublicKeyLength()) {
      ui->publickeyTextEdit->setPlainText(pubkeytemp);
    } else {
      displayError("Public key is not correct length!");
    }
  }
}
void MainWindow::on_saveprikeyButton_clicked() {
  if (ui->privatekeyTextEdit->document()->isEmpty() == false) {
    QByteArray prikeytemp = ui->privatekeyTextEdit->toPlainText().toUtf8();
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("Save Public Key"), "",
        QString("oqssec") + " file (*." + QString("oqssec") +
            ");;All Files (*)");
    if (!fileName.isEmpty()) {
      QFile file(fileName);
      if (file.open(QIODevice::WriteOnly)) {
        qDebug() << "saving";
        QJsonObject jsonObject;
        jsonObject.insert("enctype",
                          QString::number(ui->algoComboBox->currentIndex()));
        jsonObject.insert("keytype", QString::number(MainWindow::PRIVATEKEY));
        jsonObject.insert("value", QString(prikeytemp));
        QJsonDocument doc(jsonObject);
        QTextStream stream(&file);
        stream << doc.toJson();
        file.close();
      }
    }
  } else {
    displayError("No private key generated!");
  }
}

void MainWindow::on_readprikeyButton_clicked() {
  QByteArray rawjs;
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Read Public Key"), "",
      QString("oqssec") + " file (*." + QString("oqssec") + ");;All Files (*)");
  if (!fileName.isEmpty()) {
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
      qDebug() << "reading";
      QTextStream stream(&file);
      rawjs = stream.readAll().toUtf8();
      file.close();
    }
    // read enctype and set index to it
    QJsonDocument json = QJsonDocument::fromJson(rawjs);
    QJsonObject jsonObject = json.object();
    unsigned int enctype = jsonObject["enctype"].toString().toUInt();
    if (enctype > 1) {
      displayError("Wrong encoder type!");
      return;
    }
    ui->algoComboBox->setCurrentIndex(enctype);
    QString keytype = jsonObject["keytype"].toString();
    if (keytype.toUInt() != PRIVATEKEY) {
      displayError("key is not private key!");
      return;
    }
    QByteArray prikeytemp = jsonObject["value"].toString().toUtf8();
    // read in pubkeytemp if keytype is valid
    if (QByteArray::fromBase64(prikeytemp).length() ==
        asyncencoder->GetPrivateKeyLength()) {
      ui->privatekeyTextEdit->setPlainText(prikeytemp);
    } else {
      displayError("Public key is not correct length!");
    }
  }
}
void MainWindow::on_encryptandsaveButton_clicked() {
  // check if public key field and plaintext field are empty, if one of them is
  // throw error
  if (ui->publickeyTextEdit->document()->isEmpty() ||
      ui->AESplainTextEdit->document()->isEmpty()) {
    displayError("No public key and/or plaintext message written!");
    return;
  }
  QByteArray pubkey_temp =
      QByteArray::fromBase64(ui->publickeyTextEdit->toPlainText().toUtf8());
  asyncencoder->GenerateandEncodeSymKey(pubkey_temp, ciphertext,
                                        &shared_secret);
  QByteArray message_temp = ui->AESplainTextEdit->toPlainText().toUtf8();
  QByteArray tag_temp;
  tag_temp.resize(16);
  for (int i = 0; i < 12; i++) {
    aad_IV_temp.data()[i] = shared_secret[31 - i];
  }
  QByteArray ciphermessage_temp;
  ciphermessage_temp.resize(message_temp.length() * 5);
  int ciphertext_length;
  ciphertext_length =
      gcm_encrypt(reinterpret_cast<unsigned char *>(message_temp.data()),
                  message_temp.length(),
                  reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 0,
                  reinterpret_cast<unsigned char *>(shared_secret.data()),
                  reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 12,
                  reinterpret_cast<unsigned char *>(ciphermessage_temp.data()),
                  reinterpret_cast<unsigned char *>(tag_temp.data()), &p_err);
  if (p_err == false) {
    ciphermessage_temp.resize(ciphertext_length);
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("Save Encrypted Message"), "",
        tr("oqsmessage file (*.oqsmessage);;All Files (*)"));
    if (!fileName.isEmpty()) {
      QFile file(fileName);
      if (file.open(QIODevice::WriteOnly)) {
        qDebug() << "saving";
        QJsonObject encryptedmessage;
        encryptedmessage["cipherkey"] = QString(ciphertext.toBase64());
        encryptedmessage["ciphermessage"] =
            QString(ciphermessage_temp.toBase64());
        encryptedmessage["cipherhash"] = QString(tag_temp.toBase64());
        QJsonDocument JsonDocument(encryptedmessage);
        file.write(JsonDocument.toJson());
        file.close();
      }
    }
  } else {
    displayError("Error encrypting!");
  }
}
void MainWindow::on_readanddecryptButton_clicked() {
  // check if private key field is empty, throw error if so
  if (ui->privatekeyTextEdit->document()->isEmpty()) {
    displayError("No private key!");
    return;
  }
  QByteArray cipherkey;
  QByteArray ciphermessage;
  QByteArray cipherhash;
  QByteArray prikey_temp =
      QByteArray::fromBase64(ui->privatekeyTextEdit->toPlainText().toUtf8());
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Open Encrypted Message"), "",
      tr("oqsmessage file (*.oqsmessage);;All Files (*)"));
  if (!fileName.isEmpty()) {
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
      qDebug() << "reading";
      QString val;
      val = file.readAll();
      QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
      QJsonObject encryptedmessage = d.object();
      if (encryptedmessage.contains("cipherkey") &&
          encryptedmessage.contains("ciphermessage") &&
          encryptedmessage.contains("cipherhash")) {
        cipherkey = QByteArray::fromBase64(
            encryptedmessage["cipherkey"].toString().toUtf8());
        if (cipherkey.length() != asyncencoder->GetSymCipherTextLength()) {
          displayError("cipherkey is not formatted correctly");
          file.close();
          return;
        }
        ciphermessage = QByteArray::fromBase64(
            encryptedmessage["ciphermessage"].toString().toUtf8());
        cipherhash = QByteArray::fromBase64(
            encryptedmessage["cipherhash"].toString().toUtf8());
        file.close();
      } else {
        displayError("File is not formatted correctly");
        file.close();
        return;
      }
    }
    asyncencoder->DecodeSymKey(prikey_temp, cipherkey, &shared_secret);
    for (int i = 0; i < 12; i++) {
      aad_IV_temp.data()[i] = shared_secret[31 - i];
    }
    QByteArray plainmessage_temp;
    plainmessage_temp.resize(ciphermessage.length() * 5);
    int plaintext_length;
    plaintext_length = gcm_decrypt(
        reinterpret_cast<unsigned char *>(ciphermessage.data()),
        ciphermessage.length(),
        reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 0,
        reinterpret_cast<unsigned char *>(cipherhash.data()),
        reinterpret_cast<unsigned char *>(shared_secret.data()),
        reinterpret_cast<unsigned char *>(aad_IV_temp.data()), 12,
        reinterpret_cast<unsigned char *>(plainmessage_temp.data()), &p_err);
    if (p_err == false) {
      plainmessage_temp.resize(plaintext_length);
      ui->AESplainTextEdit->setPlainText(
          QString::fromUtf8(plainmessage_temp.data()));
    } else {
      displayError("Error decrypting!");
    }
  }
}
void MainWindow::on_publickeyTextEdit_textChanged() {
  if (ui->publickeyTextEdit->document()->isEmpty()) {
    ui->savepubkeyButton->setEnabled(false);
  } else {
    ui->savepubkeyButton->setEnabled(true);
  }
}
void MainWindow::on_privatekeyTextEdit_textChanged() {
  if (ui->privatekeyTextEdit->document()->isEmpty()) {
    ui->saveprikeyButton->setEnabled(false);
  } else {
    ui->saveprikeyButton->setEnabled(true);
  }
}

void MainWindow::on_algoComboBox_currentIndexChanged(int index) {
  ui->publickeyTextEdit->clear();
  ui->privatekeyTextEdit->clear();
  ui->sharedTextEdit->clear();
  ui->ciphertextTextEdit->clear();
  ui->AEScipherTextEdit->clear();
  ui->tagTextEdit->clear();
  ui->AESplainTextEdit->clear();
  asyncencoder = algoarray[index];
  public_key.resize(asyncencoder->GetPublicKeyLength());
  secret_key.resize(asyncencoder->GetPrivateKeyLength());
  ciphertext.resize(asyncencoder->GetSymCipherTextLength());
  shared_secret.resize(asyncencoder->GetSymKeyLength());
  ui->saveprikeyButton->setEnabled(false);
  ui->savepubkeyButton->setEnabled(false);
}

void handleErrors(bool *err) {
  qDebug() << "OPENSSL ERROR!";
  *err = true;
  return;
}
int MainWindow::chacha20_poly1305_encrypt(unsigned char *plaintext, int plaintext_len,
                            unsigned char *aad, int aad_len, unsigned char *key,
                            unsigned char *iv, int iv_len,
                            unsigned char *ciphertext, unsigned char *tag,
                            bool *err) {
  EVP_CIPHER_CTX *ctx;

  int len;

  int ciphertext_len = 0;
  *err = false;
  /* Create and initialise the context */
  if (!(ctx = EVP_CIPHER_CTX_new())) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise the encryption operation. */
  if (1 != EVP_EncryptInit_ex(ctx, EVP_chacha20_poly1305(), NULL, NULL, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }
  /*
   * Set IV length if default 12 bytes (96 bits) is not appropriate
   */
  if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_IVLEN, iv_len, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise key and IV */
  if (1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide any AAD data. This can be called zero or more times as
   * required
   */
  if (1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide the message to be encrypted, and obtain the encrypted output.
   * EVP_EncryptUpdate can be called multiple times if necessary
   */
  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  ciphertext_len = len;

  /*
   * Finalise the encryption. Normally ciphertext bytes may be written at
   * this stage, but this does not occur in GCM mode
   */
  if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  ciphertext_len += len;

  /* Get the tag */
  if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, 16, tag)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}
int MainWindow::chacha20_poly1305_decrypt(unsigned char *ciphertext, int ciphertext_len,
                            unsigned char *aad, int aad_len, unsigned char *tag,
                            unsigned char *key, unsigned char *iv, int iv_len,
                            unsigned char *plaintext, bool *err) {
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;
  int ret;
  *err = false;
  /* Create and initialise the context */
  if (!(ctx = EVP_CIPHER_CTX_new())) {
    handleErrors(err);
    return ciphertext_len;
  }
  /* Initialise the decryption operation. */
  if (!EVP_DecryptInit_ex(ctx, EVP_chacha20_poly1305(), NULL, NULL, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }
  /* Set IV length. Not necessary if this is 12 bytes (96 bits) */
  if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_IVLEN, iv_len, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise key and IV */
  if (!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide any AAD data. This can be called zero or more times as
   * required
   */
  if (!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide the message to be decrypted, and obtain the plaintext output.
   * EVP_DecryptUpdate can be called multiple times if necessary
   */
  if (!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  plaintext_len = len;

  /* Set expected tag value. Works in OpenSSL 1.0.1d and later */
  if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, 16, tag)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Finalise the decryption. A positive return value indicates success,
   * anything else is a failure - the plaintext is not trustworthy.
   */
  ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  if (ret > 0) {
    /* Success */
    plaintext_len += len;
    return plaintext_len;
  } else {
    /* Verify failed */
    handleErrors(err);
    return -1;
  }
}

int MainWindow::gcm_encrypt(unsigned char *plaintext, int plaintext_len,
                            unsigned char *aad, int aad_len, unsigned char *key,
                            unsigned char *iv, int iv_len,
                            unsigned char *ciphertext, unsigned char *tag,
                            bool *err) {
  EVP_CIPHER_CTX *ctx;

  int len;

  int ciphertext_len = 0;
  *err = false;
  /* Create and initialise the context */
  if (!(ctx = EVP_CIPHER_CTX_new())) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise the encryption operation. */
  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }
  /*
   * Set IV length if default 12 bytes (96 bits) is not appropriate
   */
  if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise key and IV */
  if (1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide any AAD data. This can be called zero or more times as
   * required
   */
  if (1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide the message to be encrypted, and obtain the encrypted output.
   * EVP_EncryptUpdate can be called multiple times if necessary
   */
  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  ciphertext_len = len;

  /*
   * Finalise the encryption. Normally ciphertext bytes may be written at
   * this stage, but this does not occur in GCM mode
   */
  if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  ciphertext_len += len;

  /* Get the tag */
  if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}
int MainWindow::gcm_decrypt(unsigned char *ciphertext, int ciphertext_len,
                            unsigned char *aad, int aad_len, unsigned char *tag,
                            unsigned char *key, unsigned char *iv, int iv_len,
                            unsigned char *plaintext, bool *err) {
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;
  int ret;
  *err = false;
  /* Create and initialise the context */
  if (!(ctx = EVP_CIPHER_CTX_new())) {
    handleErrors(err);
    return ciphertext_len;
  }
  /* Initialise the decryption operation. */
  if (!EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }
  /* Set IV length. Not necessary if this is 12 bytes (96 bits) */
  if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /* Initialise key and IV */
  if (!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide any AAD data. This can be called zero or more times as
   * required
   */
  if (!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Provide the message to be decrypted, and obtain the plaintext output.
   * EVP_DecryptUpdate can be called multiple times if necessary
   */
  if (!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len)) {
    handleErrors(err);
    return ciphertext_len;
  }
  plaintext_len = len;

  /* Set expected tag value. Works in OpenSSL 1.0.1d and later */
  if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag)) {
    handleErrors(err);
    return ciphertext_len;
  }

  /*
   * Finalise the decryption. A positive return value indicates success,
   * anything else is a failure - the plaintext is not trustworthy.
   */
  ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  if (ret > 0) {
    /* Success */
    plaintext_len += len;
    return plaintext_len;
  } else {
    /* Verify failed */
    handleErrors(err);
    return -1;
  }
}
void MainWindow::displayError(QString errormessage) {
  QMessageBox messageBox;
  messageBox.critical(0, "Error", errormessage);
  messageBox.setFixedSize(500, 200);
}
