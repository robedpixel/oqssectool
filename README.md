Requirements:
openssl 3.0.0+
liboqs
Qt6

If clang is being used to compile, make sure that it is installed using the visual studio installer

Instructions:

When running cmake, these variables need to point to the valid locations:
-LIBOQS_INCLUDE (the include folder for liboqs)
-OPENSSL_INCLUDE (the include folder for openssl)
-OQS_LIB (path to liboqs.lib file)
-OPENSSL_LIB (path to libcrypto.lib file)
